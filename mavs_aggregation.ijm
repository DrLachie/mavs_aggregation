var manualCount = true;
var debugMode = false;
var testFile = "F:\\testImage.tif";
var cutoff = 5;
var dir1 = "C:\\";
var dir2 = "C:\\";
var fs = File.separator();
var makeMask = true;
var countDefault = 250;
var mitoCutoff = 25; //intensity cell coveredy by signal
var redCutoff = 5; //Avg intensity of red in cell

run("Set Measurements...", "area mean standard modal min shape integrated median kurtosis area_fraction limit display redirect=None decimal=4");
run("Close All");

manualCount = getBoolean("Manual count version?");

if(debugMode){
	run("Close All");
	run("Bio-Formats Importer", "open=["+testFile+"] autoscale color_mode=Composite view=Hyperstack stack_order=XYCZT");
	processImage();
	selectWindow("MIP");
	Stack.setChannel(1);setMinAndMax(0,128);
	Stack.setChannel(2);setMinAndMax(0,128);
	Stack.setChannel(3);setMinAndMax(0,128);
	roiManager("Show All Without Labels");
}else{
	if(manualCount){
		mitoCutoff = getNumber("Set mean value of green to exclude below",25);
		Table_Heading = "Table name";
		columns = newArray("Filename","Total Cells", "Classified Cells", "Classified Positive Cells");
		table = generateTable(Table_Heading,columns);
		im = File.openDialog("Select a file");
		run("Bio-Formats Importer", "open=["+im+"] autoscale color_mode=Composite view=Hyperstack stack_order=XYCZT");
		res = processImage();	
		logResults(table,res);
		selectWindow("MIP");
		Stack.setChannel(1);setMinAndMax(0,128);
		Stack.setChannel(2);setMinAndMax(0,128);
		Stack.setChannel(3);setMinAndMax(0,128);
		run("RGB Color");
		roiManager("Show All Without Labels");
		run("Flatten");
	}else{
		main();
	}
}

function main(){
	//Setup custom results table 
	Table_Heading = "Table name";
	columns = newArray("Filename","Total Cells", "Positive Cells", "Negative Cells");
	table = generateTable(Table_Heading,columns);

	dir1 = getDirectory("Choose Directory");
	dir2 = dir1+"output"+fs;
	if(!File.exists(dir2)){
		File.makeDirectory(dir2);
	}

	list = getFileList(dir1);
	for(i=0;i<list.length;i++){
		if(endsWith(list[i],".lsm")){
			run("Bio-Formats Importer", "open=["+dir1+list[i] +"] autoscale color_mode=Composite view=Hyperstack stack_order=XYCZT");
			res = processImage();
			logResults(table,res);
			resetThreshold();
			if(makeMask){
				selectWindow("MIP");
				Stack.setChannel(1);setMinAndMax(0,128);
				Stack.setChannel(2);setMinAndMax(0,128);
				Stack.setChannel(3);setMinAndMax(0,128);
				run("RGB Color");
				roiManager("Show All Without Labels");
				run("Flatten");
				saveAs("JPG",dir2+list[i]+"_mask.jpg");
				waitForUser("check");
			}
			run("Close All");
		}
	}
}


function processImage(){
	fname = getTitle();
	//run("Z Project...", "projection=[Max Intensity]");
	rename("MIP");

	//GET NUCLEI
	run("Duplicate...", "title=nuclei duplicate channels=3");
	run("Median...", "radius=5");
	run("Subtract Background...", "rolling=50");
	
	setAutoThreshold("Triangle dark");
	setOption("BlackBackground", true);
	run("Convert to Mask");
	run("Close-");
	run("Fill Holes");
	run("Watershed");

	//GET CELL BOUNDARIES based on voronoi diaga\ram
	run("Duplicate...", "title=n2");
	run("Morphological Filters", "operation=Dilation element=Disk radius=30");
	rename("dialated");
	selectWindow("n2");
	run("Voronoi");
	setThreshold(0, 0);
	run("Convert to Mask");
	imageCalculator("AND create", "n2","dialated");
	rename("CellMask");
	run("Analyze Particles...", "display clear add");
	
	//do counting stuff
	selectWindow("MIP");
	run("Duplicate...", "title=mito duplicate channels=2");
	setAutoThreshold("Triangle dark");
	
	if(!manualCount){
		
		results = LearnedChoice(fname);
	}else{
		selectWindow("MIP");
		Stack.setChannel(1);run("Red");
		resetMinAndMax();
		Stack.setChannel(2);run("Green");
		run("Enhance Contrast", "saturated=0.35");
		Stack.setChannel(3);run("Blue");
		run("Enhance Contrast", "saturated=0.35");
		
		results = manualChoice(fname);
	}
	return results;
	
}

function manualChoice(fname){
	
	
	filterROIs();
	roiManager("Show None");
	
	totalCells = roiManager("Count");
	howManyToClassify = getNumber("There are "+totalCells+" in this image. How many do you want to classify?",250);
	countedPos = 0;
	while(howManyToClassify > totalCells){
		howManyToClassify = getNumber("Too many. How many?",countDefault);
	}
	selectionArray = generateRandomArray(howManyToClassify, totalCells);
	for(i=0;i<howManyToClassify;i++){
		roiManager("Select",selectionArray[i]);
		run("Duplicate...", "duplicate");
		rename("temp");
		run("Scale...", "x=5 y=5 z=1.0 create");
		Stack.setChannel(2);run("Green");resetMinAndMax();
		Stack.setChannel(1);run("Blue");resetMinAndMax();
		if(getBoolean("Positive?")){
			countedPos++;
			roiManager("Set Color","Green");
			roiManager("Set Line Width",3);
		}else{
			roiManager("Set Color","Red");
			roiManager("Set Line Width",3);
		}
		close("temp*");
		selectWindow("MIP");
	}
	returnArray = newArray(fname,totalCells,howManyToClassify,countedPos);
	return returnArray;
}



function measureRegions(fname){
	nRegions = roiManager("Count");
	run("Clear Results");
	posCells = 0;
	negCells = 0;
	for(i=0;i<nRegions;i++){
		roiManager("Select",i);
		run("Measure");		
		mean = getResult("Mean");
		kurt = getResult("Kurt");
		if(mean>cutoff && kurt<2.0){
			roiManager("Set Color","Red");
			posCells++;
		}
		if(isNaN(mean)){
			roiManager("Set Color","Blue");
			negCells++;
		}
	}

	counts = newArray(fname,nRegions,posCells,negCells);
	return counts;
}


//Generate a custom table
//Give it a title and an array of headings
//Returns the name required by the logResults function
function generateTable(tableName,column_headings){
	if(isOpen(tableName)){
		selectWindow(tableName);
		run("Close");
	}
	tableTitle=tableName;
	tableTitle2="["+tableTitle+"]";
	run("Table...","name="+tableTitle2+" width=600 height=250");
	newstring = "\\Headings:"+column_headings[0];
	for(i=1;i<column_headings.length;i++){
			newstring = newstring +" \t " + column_headings[i];
	}
	print(tableTitle2,newstring);
	return tableTitle2;
}


//Log the results into the custom table
//Takes the output table name from the generateTable funciton and an array of resuts
//No checking is done to make sure the right number of columns etc. Do that yourself
function logResults(tablename,results_array){
	resultString = results_array[0]; //First column
	//Build the rest of the columns
	for(i=1;i<results_array.length;i++){
		resultString = toString(resultString + " \t " + results_array[i]);
	}
	//Populate table
	print(tablename,resultString);
}

function generateRandomArray(length,maxVal){
	randArray = newArray(length);
	for(i=0;i<length;i++){
		val = round(random()*maxVal);
		while(isIn(val,randArray)){
			val = round(random()*maxVal);
		}
		randArray[i] = val;
	}
	return randArray;
}

function isIn(val,arr){
	itsIn = false;
	for(i=0;i<arr.length;i++){
		if(val == arr[i]){
			itsIn = true;
			i = arr.length;
		}
	}
	return itsIn;	
}

function filterROIs(){
	run("Clear Results");
	selectWindow("mito");
	roiManager("Measure");
	//filter on mito area (already thresholded);
	deleteArray = newArray();
	for(i=0;i<nResults();i++){
		if((getResult("Mean",i)<mitoCutoff) || isNaN(getResult("Mean",i))){
			print(getResult("Mean",i));
			deleteArray = Array.concat(deleteArray,i);
		}
	}
	Array.print(deleteArray);
	roiManager("Select",deleteArray);
	roiManager("Delete");
	selectWindow("MIP");
	
	return true;
}

function saveMask(name){
		selectWindow("MIP");
		Stack.setChannel(1);setMinAndMax(0,128);
		Stack.setChannel(2);setMinAndMax(0,128);
		Stack.setChannel(3);setMinAndMax(0,128);
		run("RGB Color");
		roiManager("Show All Without Labels");
		run("Flatten");
}
